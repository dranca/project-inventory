﻿using UnityEngine;
using System.Collections;

public class BagPack:ScriptableObject{

	public MovableObject[,] bag;
	public int bagSize = 5;
	public GameObject drawableBag;

	public BagPack()
	{
		bag = new MovableObject[4,bagSize / 4 + bagSize % 4];
	}
}
