﻿using UnityEngine;
using System.Collections;

public class BagManager : MonoBehaviour {

	public GUITexture bagPackBAckground;
	private ArrayList shownBags;
	public ArrayList bags;
	public bool bagsAreSHown = false;
	// Use this for initialization
	void Start () {
		this.bags = new ArrayList();
		for( int i = 0 ; i < 4 ; i++)
		{
			bags.Add(ScriptableObject.CreateInstance<BagPack>());
		}
		this.initializeBagsTextures();
	}

	void initializeBagsTextures()
	{
		int height = 25;
		int inset = 3;
		int witdh = 25;
		foreach(BagPack bag in bags)
		{
			GameObject window = new GameObject();
			bag.drawableBag = window;
			for(int i = 0 ; i < bag.bag.GetLength(0);i++)
			{
				for (int j = 0 ; j < bag.bag.GetLength(1); j ++)
				{
					Vector3 position = Vector3.zero;
					position.z = Camera.main.transform.position.z;
					GUITexture texture = (GUITexture) Object.Instantiate(bagPackBAckground,position,Quaternion.identity);
					texture.pixelInset = new Rect(i*(witdh + inset) ,j * (height + inset), witdh , height);
					texture.transform.parent = window.transform;
				}
			}
			window.SetActive(false);
		}
	}

	// i will use this method to show the backpack.
	public void showBags(bool show,int number)
	{
			foreach(BagPack bag in bags)
			{
				bag.drawableBag.SetActive(show);
			}
	}

	void OnGUI()
	{
		if(GUI.Button(new Rect(10,10,100,100),"show"))
		{
			bagsAreSHown = !bagsAreSHown;
			this.showBags(bagsAreSHown,0);

		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
