﻿using UnityEngine;
using System.Collections;

public class MovableObject : MonoBehaviour {


	public ArrayList objectType;
	public int objectId;
//	public ArrayList<DAFPlaceableObjects> objectType;
	public DAFPlaceableRarity objectRarity;
	public Texture2D objectImage;


	public enum DAFPlaceableObjects
	{
		bag,
		ecquipment,
		consumable,
		craftingItem,
		craftedItem,
		trash
	}

	public enum DAFPlaceableRarity
	{
		trash,
		white,
		green,
		blue,
		purple,
		legendary
	}
	private Vector3 dragPosition;
	
	void Start () {
		
	}
}
