﻿using UnityEngine;
using System.Collections;

public class BagManager : MonoBehaviour {

	public GUITexture bagPackBAckground;
	private ArrayList shownBags;
	public ArrayList bags;
	public bool bagsAreSHown = false;

	public MovableObject movableObject;

	// Use this for initialization
	void Start () {
		this.bags = new ArrayList();
		for( int i = 0 ; i < 4 ; i++)
		{
			Debug.Log("aass");
			BagPack bagPack = new BagPack();
			bagPack.parentManager = this;
			bagPack.init();
			bags.Add(bagPack);
			Debug.Log("aassasdasda");
		}
		this.initializeBagsTextures();
	}

	void initializeBagsTextures()
	{
		int height = 25;
		int inset = 3;
		int witdh = 25;
		int bagNumber = 0;
		foreach(BagPack bag in bags)
		{

			GUITexture windowTexture;
			if(bag.drawableBag == null)
			{
				windowTexture = (GUITexture) Object.Instantiate(bagPackBAckground,Vector3.zero,Quaternion.identity);
				windowTexture.pixelInset = new Rect(Screen.width - 130,bagNumber * (60 + 10),110,60);
				GameObject gameObjects = new GameObject();
				bag.drawableBag = gameObjects;
				bag.bagBackGround = windowTexture;
				windowTexture.transform.parent = gameObjects.transform;
				gameObjects.SetActive(false);
			}
			else 
			{
				windowTexture = bag.bagBackGround;
			}

			for(int i = 0 ; i < bag.bag.GetLength(0);i++)
			{
				for (int j = 0 ; j < bag.bag.GetLength(1); j ++)
				{
					Vector3 position = Vector3.zero;
					position.z = 1;
					GUITexture texture = (GUITexture) Object.Instantiate(bagPackBAckground,position,Quaternion.identity);
					texture.pixelInset = new Rect(i*(witdh + inset) + windowTexture.pixelInset.x ,j * (height + inset) + windowTexture.pixelInset.y, witdh , height);
					texture.transform.parent = windowTexture.transform;
					if(bag.bag[i,j] != null)
					{
						Debug.Log("asdasd " + j + i );
						Vector3 pos = Vector3.zero;
						pos.z = 2;

						// create the background: i choose to redraw it every time we add some other new item into background
						GUITexture textureObject = (GUITexture) Object.Instantiate(movableObject.objectImage,pos,Quaternion.identity);
						// 
						textureObject.transform.parent = texture.transform.parent;
						textureObject.pixelInset = new Rect(texture.pixelInset.position.x + 2,texture.pixelInset.position.y + 2, texture.pixelInset.width - 4, texture.pixelInset.height - 4);


						MovableObject movableForPosition = (MovableObject)bag.bag[i,j];
						movableForPosition.objectImage = textureObject;
						textureObject.transform.position = pos;
					}
				}
			}
			bagNumber ++;
//			windowTexture.SetActive(false);
		}
	}


	public void addObject()
	{
		MovableObject mObject = GameObject.Instantiate (this.movableObject)as MovableObject;
//		mObject.transform.parent = this.transform;
		this.addObjectToBags (mObject);
		this.initializeBagsTextures();
	}

	public void addObjectToBags(MovableObject movableObjectInstance)
	{
		foreach(BagPack bt in this.bags)
		{
			for(int j = 0 ; j < bt.bag.GetLength(1); j ++)
			{
				for(int i = 0 ; i < bt.bag.GetLength(0); i++)
				{
					if(bt.bag[i,j] == null)
					{
						bt.bag[i,j] = movableObjectInstance;
//						movableObjectInstance.transform.parent = bt.
						return;
					}
				}
			}
		}
		Debug.LogWarning ("you are out of bag spaces");
	}
	// i will use this method to show the backpack.
	public void showBags(bool show,int number)
	{

		if(number == 0)
			foreach(BagPack bag in bags)
			{
				bag.drawableBag.SetActive(show);
			}
		else 
		{
			BagPack bag =(BagPack) bags[number - 1];
			bag.drawableBag.SetActive(show);
		}
	}

	void OnGUI()
	{
		if(GUI.Button(new Rect(10,10,100,100),"show"))
		{
			bagsAreSHown = !bagsAreSHown;
			this.showBags(bagsAreSHown,0);

		}

		if (GUI.Button (new Rect (10, 110, 100, 100), "Add Object")) {
			this.addObject();
		
		}

	}

	// Update is called once per frame
	void Update () {
	
	}
}
